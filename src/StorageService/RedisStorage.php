<?php

namespace App\StorageService;

use App\ExternalClient\AppRedisClient;

final class RedisStorage
{
    private \Redis $redis;

    public function __construct(
        AppRedisClient $appRedisClient,
    )
    {
        $this->redis = $appRedisClient->getRedis();
    }

    public function get(string $hash): ?string
    {
        $url = $this->redis->get($hash);

        return is_string($url) ? $url : null;
    }

    public function save(string $hash, string $url): bool
    {
        /**
         * Set key to hold string value if key does not exist
         * @link https://redis.io/commands/setnx
         */
        return $this->redis->setnx($hash, $url);
    }
}
