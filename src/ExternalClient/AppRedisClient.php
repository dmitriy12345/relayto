<?php

namespace App\ExternalClient;

/**
 * Usefull for autowire
 */
final class AppRedisClient
{
    private \Redis $redis;

    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    public function getRedis(): \Redis
    {
        return $this->redis;
    }
}
