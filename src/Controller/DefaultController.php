<?php

namespace App\Controller;

use App\StorageService\RedisStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController extends AbstractController
{
    public function __construct(
        private RedisStorage $redisStorage,
    )
    {
    }

    /**
     * Accept POST with json encoded request
     *
     * @Route(path="/encode", name="encode", methods={"POST"})
     */
    public function encode(Request $request): JsonResponse
    {
        $rawContent = $request->getContent();
        try {
            $data = json_decode($rawContent, true, 512, JSON_THROW_ON_ERROR);

            if (empty($data['url'])
                || false === filter_var($data['url'], FILTER_VALIDATE_URL)
            ) {
                throw new \InvalidArgumentException('Url is not valid: ' . ( $data['url'] ?? '' ));
            }

            $hash = dechex(crc32($data['url']));
            $this->redisStorage->save($hash, $data['url']);

            $response['hash'] = $hash;
            $response['success'] = true;

            return new JsonResponse($response);

        } catch (\Throwable $throwable) {
            return $this->createExceptionResponse($throwable);
        }
    }

    /**
     * Accept POST with json encoded request
     *
     * @Route(path="/decode", name="decode", methods={"POST"})
     */
    public function decode(Request $request): JsonResponse
    {
        $rawContent = $request->getContent();
        try {
            $data = json_decode($rawContent, true, 512, JSON_THROW_ON_ERROR);

            $hashLength = 8; // because created from 32 bit integer
            if (empty($data['hash']) || $hashLength !== strlen($data['hash'])) {
                throw new \InvalidArgumentException('Hash is not valid: ' . ( $data['hash'] ?? '' ));
            }

            $url = $this->redisStorage->get($data['hash']);

            $response['url'] = $url;
            $response['success'] = true;

            return new JsonResponse($response);

        } catch (\Throwable $throwable) {
            return $this->createExceptionResponse($throwable);
        }
    }

    private function createExceptionResponse(\Throwable $throwable): JsonResponse
    {
        return new JsonResponse(
            [
                'error' => $throwable->getMessage(),
                'success' => false,
            ],
            Response::HTTP_BAD_REQUEST
        );
    }
}
