<?php

namespace App\Tests\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ApiDecodeTest extends AbstractApiTest
{
    public function testSuccessDecode(): void
    {
        // add url
        $this->makeRequest(Request::METHOD_POST, self::ENCODE_URL, $this->getDataToEncode());
        // get by hash
        $this->makeRequest(Request::METHOD_POST, self::DECODE_URL, $this->getDataToDecode());

        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $content = $this->httpClient->getResponse()->getContent();
        $responseData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals(self::URL_TEST, $responseData['url']);
    }

    public function testEmptyValueDecode(): void
    {
        $this->makeRequest(Request::METHOD_POST, self::DECODE_URL);
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $content = $this->httpClient->getResponse()->getContent();
        $responseData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals('Hash is not valid: ', $responseData['error']);
    }
}
