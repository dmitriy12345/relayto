<?php

namespace App\Tests\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ApiEncodeTest extends AbstractApiTest
{
    public function testSuccessEncode(): void
    {
        $this->makeRequest(Request::METHOD_POST, self::ENCODE_URL, $this->getDataToEncode());

        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $content = $this->httpClient->getResponse()->getContent();
        $responseData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals(self::URL_HASH, $responseData['hash']);
    }

    public function testEmptyValueEncode(): void
    {
        $this->makeRequest(Request::METHOD_POST, self::ENCODE_URL);
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $content = $this->httpClient->getResponse()->getContent();
        $responseData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals('Url is not valid: ', $responseData['error']);
    }
}
