<?php

namespace App\Tests\Api;

use JetBrains\PhpStorm\ArrayShape;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractApiTest extends WebTestCase
{
    protected const ENCODE_URL = '/encode';
    protected const DECODE_URL = '/decode';

    protected const URL_TEST = 'https://testsite.com/test';
    protected const URL_HASH = 'c1866122';

    protected KernelBrowser $httpClient;

    public function setUp(): void
    {
        $this->httpClient = self::createClient();
    }

    protected function makeRequest(string $type, string $url, array $data = [], array $files = []): void
    {
        $this->httpClient->request(
            $type,
            $url,
            [],
            $files,
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data, JSON_THROW_ON_ERROR, 512)
        );
    }

    #[ArrayShape(['url' => "string"])]
    protected function getDataToEncode(): array
    {
        return ['url' => self::URL_TEST];
    }

    #[ArrayShape(['hash' => "string"])]
    protected function getDataToDecode(): array
    {
        return ['hash' => self::URL_HASH];
    }
}
