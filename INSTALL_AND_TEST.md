### RelayTo test app

URL shortening service using PHP and Symfony.

### Requirements

* PHP 8.0 or higher
* **php8.0-redis** library ```$ apt install php8.0-redis```
* **Optional:** Redis or Docker with Redis container

### Install php dependencies
1) run ```composer install``` for development
   or ```composer install --no-dev``` for production
2) run ```composer dump-env dev``` not required, only for better performance

### Run test
```$ php bin/phpunit```

### Important
The application is configured to use a remote Redis server by default ```admin.lisogor.com:46379```

Web admin console for remote Redis https://admin.samborskiy.com/RedisAdminRelayto/

Fully functional version is available here (POST JSON):

https://admin.lisogor.com/encode

https://admin.lisogor.com/decode

**Please note:**

Redis database index for app: 1 (edit in .env)

Redis database index for test: 2 (edit in .env.test)


### OPTIONAL: Install Redis container locally

1) copy ```docker-compose.yml.dist``` -> ```docker-compose.yml```
2) run ```docker-compose up -d --build```
